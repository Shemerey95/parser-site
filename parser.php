<?php

use Bitrix\Main\Loader;

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

Loader::includeModule('iblock');

require_once('local/phpQuery.php');

//формируем список разделов инфоблока
$htmlSections = file_get_contents('http://expertsouth.ru/specproekty/');
$domSections  = phpQuery::newDocument($htmlSections);
$sections     = $domSections->find('.categorylist .subcat');

foreach ($sections as $sectionElement) {
    $pqSections   = pq($sectionElement);
    $sectionTitle = $pqSections->find('a')->text();
    $linkSections = $pqSections->find('a')->attr('href');
    $pqSections->find('a')->prepend('<br />');

    $sectionCode = str_replace('/specproekty/', '', $linkSections);

    // Добавляем в бд все подразделы инфоблока
    $elSection = new CIBlockSection();

    $arLoadProductArray = [
        'IBLOCK_SECTION_ID' => false,
        'IBLOCK_ID'         => 104,
        'NAME'              => $sectionTitle,
        'ACTIVE'            => 'Y',
        'CODE'              => $sectionCode,
        'IBLOCK_TYPE_ID'    => 'specproekty',
        'SORT'              => '500',
    ];
    $elSection->Add($arLoadProductArray);

    $linkSections = 'http://expertsouth.ru/' . $linkSections;

    parserSection($linkSections, $sectionTitle);
}

// выбираем из бд ID всех подразделов нужного нам раздела
function getSectionId($sectionTitle)
{
    $query = new Bitrix\Main\Entity\Query(
        Bitrix\Iblock\SectionTable::getEntity()
    );
    $query->setSelect(
        [
            'ID',
            'NAME',
        ]
    )
        ->setFilter(['IBLOCK_ID' => 104, 'NAME' => $sectionTitle, 'ACTIVE' => 'Y']);

    $result = $query->exec();

    while ($arItems = $result->fetch()) {
        $sectionList[] = $arItems;
    }

    return $sectionList[0]['ID'];
}

// парсинг страницы списка элементов
function parserSection($linkSections, $sectionTitle)
{
    $htmlSection       = file_get_contents($linkSections);
    $domSection        = phpQuery::newDocument($htmlSection);
    $domSectionElement = $domSection->find('.contentlist .category_content');

    foreach ($domSectionElement as $element) {
        $pqSection   = pq($element);
        $linkElement = $pqSection->find('.content_title .con_titlelink')->attr('href');
        $previewImg  = $pqSection->find('.content_other_info .content_image a img')->attr('src');
        $previewText = $pqSection->find('.content_other_info p span');
        $linkElement = "http://expertsouth.ru/" . $linkElement;
        $previewImg  = CFile::MakeFileArray('http://expertsouth.ru/' . $previewImg);

        parseElement($linkElement, $sectionTitle, $previewImg, $previewText);
    }
    // if ($domPageNavigation) {
    //     $navigation     = pq($domPageNavigation);
    //     $lastNumberPage = $navigation->find('.pagebar_page::last-child')->text();
    //     $page           = 2;
    //     while ($page <= $lastNumberPage) {
    //         $linkSections = substr($linkSections, 0, strrpos($linkSections, '/', -1));
    //         $linkSections = $linkSections . "/page-" . $page;
    //         parserSection($linkSections, $sectionTitle);
    //         $page++;
    //     }
    // }
}

// парсинг детальной страницы статьи
function parseElement($linkElement, $sectionTitle, $previewImg, $previewText)
{
    $htmlElement = file_get_contents($linkElement);
    $domElement  = phpQuery::newDocument($htmlElement);

    $pqElement          = pq($domElement);
    $titleDetailElement = $pqElement->find('#print .con_heading')->text();
    $textElement        = $pqElement->find('#print .con_text')->html();
    $elementCode        = str_replace('.html', '', str_replace('/', '', strrchr($linkElement, '/')));
    $imgElement         = CFile::MakeFileArray('http://expertsouth.ru' . $pqElement->find('#print .con_text .con_image_main .fancybox-effects-d img')->attr('src'));
    $pubDate            = $pqElement->find('#print .subheadarticle .con_info .pubdate')->text();
    $pubDate            = str_replace(' |', '', $pubDate);

    $PROP[]          = '';
    $PROP['PUBDATE'] = $pubDate;

    $elSection = new CIBlockElement();

    $arLoadProductArray = [
        'IBLOCK_SECTION_ID' => getSectionId($sectionTitle),
        'IBLOCK_ID'         => 104,
        'NAME'              => $titleDetailElement,
        'ACTIVE'            => 'Y',
        'DETAIL_PICTURE'    => $imgElement,
        'PREVIEW_PICTURE'   => $previewImg,
        'PREVIEW_TEXT'      => $previewText,
        'DETAIL_TEXT'       => $textElement,
        'CODE'              => $elementCode,
        'PROPERTY_VALUES'   => $PROP,
        'SORT'              => '500',
    ];

    $elSection->Add($arLoadProductArray);
}

phpQuery::unloadDocuments();
